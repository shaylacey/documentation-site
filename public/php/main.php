<?php 
    function parse(){
        $parsedown = new Parsedown();
        $files = scandir('md/');
        $pattern = "/^([\d]+)__(.*)$/";
        $sorting = array();
        natsort($files);
        foreach ($files as $file){
            if ($file != "." && $file != ".."){
                preg_match($pattern, $file, $results);
                natsort($sorting);
                array_push($sorting, $results[2]);
            }
        }
        foreach ($files as $file){
            if ($file != "." && $file != ".."){
                $title = $file;
                $title=substr($title, 0, (strlen ($title)) - (strlen (strrchr($file,'.'))));
                $id = $title;
                $id = str_replace(" ", "-", $id);
                $id = str_replace(".txt", "", $id);
                $id = str_replace("__", "-", $id);
                echo '<div id=' . "$id" . '>';
                echo $parsedown->text(file_get_contents("md/$file"));
                echo '</div>';
            }
        }
    }

    function titles(){
        $files = scandir('md/');
        $pattern = "/^([\d]+)__(.*)$/";
        $sorting = array();
        foreach ($files as $file){
            if ($file != "." && $file != ".."){
                preg_match($pattern, $file, $results);
                array_push($sorting, $results[0]);
                natsort($sorting);
            }
        }
        foreach ($sorting as $title){
            if ($file != "." && $file != ".."){
                $title=str_replace(".txt", "", $title);
                $title = str_replace("__", " ", $title);
                $id = $title;
                $id = str_replace(" ", "-", $id);
                $id = str_replace(".txt", "", $id);
                $id = str_replace("__", "-", $id);
                echo "<li>";
                echo '<a href="' . "#" . "$id".  '" class="white-bg">'. $title. "</a>";
                echo "</li>";
            }
        }
    }