<html>
        @include("layouts.partials.sections.head")
        <div class="outer__main__container grid grid-large">
                @include("layouts.partials.sections.header")
                <div class="main__inner__container grid grid-med" style="grid-column: 1 / -1;" id="mobilemenu">
                        @include("layouts.partials.content.write")
                </div>
        </div>
</html>