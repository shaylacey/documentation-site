<!DOCTYPE html>
<html>
        @include("layouts.partials.sections.head")
        <body>
                @include("layouts.partials.sections.header")

                @include("layouts.partials.sections.sidebar")
                
                <div class="main__container">
                        @include("layouts.partials.content.index")
                        <div class="bg__styled__container"></div>
                </div>
        </body>
</html>