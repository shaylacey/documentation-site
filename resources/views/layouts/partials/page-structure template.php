
<div class="outer__main__container grid grid-large">
    @include("layouts.partials.sections.header")

    @include("layouts.partials.sections.sidebar")

    <div class="main__inner__container">
            @include("layouts.partials.content.index")
    </div>

</div>


