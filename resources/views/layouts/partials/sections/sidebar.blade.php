<div class="sidebar h100 white-bg flex fcol" id="sidebar">
  <input type="text" id="searchbar" onkeyup="search()" class="w100 tcenter fjcenter b0 bt bb" style="height:35px;" placeholder="Search for section" title="Type in a section">
  <ul id="TitleList">
  <?
    echo titles();
  ?>
  </ul>
  <script>
    function search() {
        var input, filter, ul, li, a, i;
        input = document.getElementById("searchbar");
        filter = input.value.toUpperCase();
        ul = document.getElementById("TitleList");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }
    </script>
</div>