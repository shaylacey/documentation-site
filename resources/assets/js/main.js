function mobmenu(){
    document.getElementById("closemenubut").classList.add("show");
    document.getElementById("closemenubut").classList.remove("dns","menu","hide");
    document.getElementById("menubut").classList.add("hide","dns","hide");
    document.getElementById("menubut").classList.remove("mmenu","limit");
    document.getElementById("stylecontainer").classList.add("scontainer");
    sBar();
}
function closemobmenu(){
    document.getElementById("closemenubut").classList.remove("show");
    document.getElementById("closemenubut").classList.add("dns", "menu", "hide");
    document.getElementById("menubut").classList.remove("hide","dns","menu");
    document.getElementById("menubut").classList.add("limit");
    document.getElementById("stylecontainer").classList.remove("scontainer");
    closeSB();
}

function sBar(){
    document.getElementById("sidebar").classList.add("o_sb");
    document.getElementById("hide").classList.add("o_mic");
}

function closeSB(){
    document.getElementById("sidebar").classList.remove("o_sb");
    document.getElementById("hide").classList.remove("o_mic");
}