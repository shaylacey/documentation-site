<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', function () {
    return view('layouts.app');
});
Route::get('/', function () {
    return view('layouts.app');
});

Route::get('/write', function (){
    return view('layouts.write');
});

Route::get('/prism', function (){
    return view('layouts.prism');
});

